/*
 * hx711.c
 *
 *  Created on: 05.08.2019
 *      Author: domino-pl
 */

#include "hx711.h"
#include "math.h"

inline void __hx711_SCK_Tact(const HX711_t * hx711){
	HAL_GPIO_WritePin(hx711->SCK_Port, hx711->SCK_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(hx711->SCK_Port, hx711->SCK_Pin, GPIO_PIN_RESET);
}

void hx711_Reset(HX711_t * hx711){
	hx711->ReadingNow = true;
	HAL_GPIO_WritePin(hx711->SCK_Port, hx711->SCK_Pin, GPIO_PIN_SET);
	HAL_Delay(1);
	hx711->ReadingNow = false;
	HAL_GPIO_WritePin(hx711->SCK_Port, hx711->SCK_Pin, GPIO_PIN_RESET);
}

void hx711_IT_Interrapt_Disabling(HX711_t * hx711, const IRQn_Type DT_PIN_ItIRQn){
	hx711->It_DT_PIN_ItIRQn = DT_PIN_ItIRQn;
	__hx711_Enable_Disable_DT_PIN_Interrapt(hx711);
}

void __hx711_Enable_Disable_DT_PIN_Interrapt(HX711_t * hx711){
	if(hx711->It_DT_PIN_ItIRQn > 0){
		if(hx711->ItMeasurementsEnabled)
			HAL_NVIC_EnableIRQ(hx711->It_DT_PIN_ItIRQn);
		else
			HAL_NVIC_DisableIRQ(hx711->It_DT_PIN_ItIRQn);
	}
}

void hx711_Init(HX711_t * hx711, GPIO_TypeDef * hx711_SCK_Port, const uint16_t hx711_SCK_Pin, GPIO_TypeDef * hx711_DT_Port, const uint16_t hx711_DT_Pin){
	hx711->SCK_Port = hx711_SCK_Port;
	hx711->SCK_Pin = hx711_SCK_Pin;
	hx711->DT_Port = hx711_DT_Port;
	hx711->DT_Pin = hx711_DT_Pin;

	hx711->Gain = HX711_Gain_A128;
	hx711->RangeExceedance = HX711_Range_Exceedance_OK;

	hx711->ReadingNow = false;
	hx711->TareOffset = 0;
	hx711->ScaleFactor = 0;
	hx711->ScaleFactorUncertainty = 0;

	hx711->ItLastReadingRaw = 0;
	hx711->ItNewMeasurement = false;

	hx711->It_DT_PIN_ItIRQn = 0;

	hx711_IT_Measurements_Disable(hx711);
	hx711_Reset(hx711);
	return;
}

void hx711_IT_Measurements_Enable(HX711_t * hx711, const uint8_t mesurements){
	hx711->__ItMeasurementsI = 0;
	hx711->__ItMeasurementAvgSum = 0;
	hx711->ItMeasurementsNumber = mesurements;
	hx711->ItMeasurementsEnabled = true;
	__hx711_Enable_Disable_DT_PIN_Interrapt(hx711);
	if(hx711_Ready(hx711))
		hx711_IT_Interrupt_Callback(hx711, hx711->DT_Pin);
}

void hx711_IT_Measurements_Disable(HX711_t * hx711){
	hx711->ItMeasurementsEnabled = false;
	__hx711_Enable_Disable_DT_PIN_Interrapt(hx711);
}


void hx711_Set_Gain(HX711_t * hx711, const HX711_Gain_t new_gain){
	hx711->Gain = new_gain;
}

bool hx711_Ready(const HX711_t * hx711){
	return !HAL_GPIO_ReadPin(hx711->DT_Port, hx711->DT_Pin);
}

uint32_t __hx711_Download_Data(HX711_t * hx711){
	hx711->ReadingNow = true;

	uint32_t read = 0;
	for(uint8_t i=0; i<24; i++){
		__hx711_SCK_Tact(hx711);
		read <<= 1;
		if(HAL_GPIO_ReadPin(hx711->DT_Port, hx711->DT_Pin))
			read += 1;
	}
	for(uint8_t i=0; i<hx711->Gain; i++)
		__hx711_SCK_Tact(hx711);
	read ^= 0x800000;

	#if HX711_ENABLE_RANGE_EXCEEDANCE_CHECKING == YES
	if(read == 0x000000)
		hx711->RangeExceedance |= HX711_Range_Exceedance_Bottom;
	else if(read == 0xffffff)
		hx711->RangeExceedance |= HX711_Range_Exceedance_Top;
	#endif

	hx711->ReadingNow = false;
	return read;
}

float hx711_Read(HX711_t * hx711, const uint8_t measurements){
	return hx711_Calc(hx711, hx711_Read_Raw(hx711, measurements));
}

uint32_t hx711_Read_Raw(HX711_t * hx711, const uint8_t measurements){
	// In order to increase maximum measurements value change "sum" type to uint64_t
	// and remember to set same type for "measurements" and "i" variables.
	uint32_t sum = 0;
	for(uint8_t i=0; i<measurements; i++){
		while(HAL_GPIO_ReadPin(hx711->DT_Port, hx711->DT_Pin));
		sum += __hx711_Download_Data(hx711);
	}
	return (uint32_t)(sum/measurements);
}

void hx711_Tare(HX711_t * hx711, const uint8_t measurements){
	hx711->TareOffset = hx711_Read_Raw(hx711, measurements);
}

void hx711_Scale(HX711_t * hx711, const float sample, const uint8_t measurements){
	hx711->ScaleFactor = sample/(hx711_Read_Raw(hx711, measurements)-hx711->TareOffset);
}

float hx711_Calc(HX711_t * hx711, const uint32_t raw_value){
	return ((float)raw_value-(float)hx711->TareOffset)*hx711->ScaleFactor;
}

void hx711_IT_Interrupt_Callback(HX711_t * hx711, const uint16_t GPIO_Pin){
	if(GPIO_Pin == hx711->DT_Pin){
		 if(hx711->ItMeasurementsEnabled && !hx711->ReadingNow && hx711_Ready(hx711)){
			 if(hx711->ItMeasurementsNumber == 1){
				 hx711->ItLastReadingRaw = __hx711_Download_Data(hx711);
				 hx711->ItNewMeasurement = true;
			 }else{
				 hx711->__ItMeasurementAvgSum += __hx711_Download_Data(hx711);
				 ++(hx711->__ItMeasurementsI);
				 if(hx711->__ItMeasurementsI == hx711->ItMeasurementsNumber){
					 hx711->ItLastReadingRaw = hx711->__ItMeasurementAvgSum/hx711->ItMeasurementsNumber;
					 hx711->__ItMeasurementsI = 0;
					 hx711->__ItMeasurementAvgSum = 0;
					 hx711->ItNewMeasurement = true;
				 }
			 }
		 }
	 }
}

float hx711_IT_Read(HX711_t * hx711, bool * new_mesurement){
	if(new_mesurement != NULL)
		*new_mesurement = hx711->ItNewMeasurement;
	hx711->ItNewMeasurement = false;
	return hx711_Calc(hx711, hx711->ItLastReadingRaw);
}
