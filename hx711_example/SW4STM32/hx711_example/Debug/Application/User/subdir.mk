################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/User/hx711.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Src/main.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Src/stm32f1xx_hal_msp.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Src/stm32f1xx_it.c 

OBJS += \
./Application/User/hx711.o \
./Application/User/main.o \
./Application/User/stm32f1xx_hal_msp.o \
./Application/User/stm32f1xx_it.o 

C_DEPS += \
./Application/User/hx711.d \
./Application/User/main.d \
./Application/User/stm32f1xx_hal_msp.d \
./Application/User/stm32f1xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
Application/User/%.o: ../Application/User/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/main.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Src/main.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f1xx_hal_msp.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Src/stm32f1xx_hal_msp.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f1xx_it.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Src/stm32f1xx_it.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


