################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c \
/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_uart.c 

OBJS += \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_cortex.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_dma.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_exti.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_flash.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_flash_ex.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_gpio.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_gpio_ex.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_pwr.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_rcc.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_rcc_ex.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_tim.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_tim_ex.o \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_uart.o 

C_DEPS += \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_cortex.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_dma.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_exti.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_flash.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_flash_ex.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_gpio.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_gpio_ex.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_pwr.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_rcc.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_rcc_ex.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_tim.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_tim_ex.d \
./Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_uart.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_cortex.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_dma.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_exti.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_flash.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_flash_ex.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_gpio.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_gpio_ex.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_pwr.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_rcc.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_rcc_ex.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_tim.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_tim_ex.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Drivers/STM32F1xx_HAL_Driver/stm32f1xx_hal_uart.o: /media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_uart.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F103xB -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/SW4STM32/hx711_example/Application/User/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"/media/veracrypt2/repos/stm32hx711/hx711_example/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


