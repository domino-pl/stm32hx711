/*
 * hx711.h
 *
 *  Created on: 05.08.2019
 *      Author: domino-pl
 */

#ifndef _HX711_H_
#define _HX711_H_

#include "stm32f1xx_hal.h"
#include <stdbool.h>

#define HX711_ENABLE_RANGE_EXCEEDANCE_CHECKING NO
typedef uint8_t HX711_Range_Exceedance_t;
#define HX711_Range_Exceedance_OK (0)
#define HX711_Range_Exceedance_Top (1)
#define HX711_Range_Exceedance_Bottom (2)

typedef uint8_t HX711_Gain_t;
#define HX711_Gain_A128 (1)
#define HX711_Gain_B32 (2)
#define HX711_Gain_A64 (3)


struct HX711_struct{
	GPIO_TypeDef * SCK_Port;
	uint16_t SCK_Pin;
	GPIO_TypeDef * DT_Port;
	uint16_t DT_Pin;

	HX711_Gain_t Gain;

	uint32_t TareOffset;
	float ScaleFactor;
	float ScaleFactorUncertainty;

	HX711_Range_Exceedance_t RangeExceedance; // Informs about RangeEcceedence event
	bool ReadingNow;

	bool ItMeasurementsEnabled;
	uint8_t ItMeasurementsNumber; // Number of elements in measurements average
	uint8_t __ItMeasurementsI; // Already added elements to current average
	uint32_t __ItMeasurementAvgSum; // Current average sum
	uint32_t ItLastReadingRaw;
	bool ItNewMeasurement;
	IRQn_Type It_DT_PIN_ItIRQn;

};

typedef struct HX711_struct HX711_t;

void hx711_Reset(HX711_t * hx711);
void hx711_Init(HX711_t * hx711, GPIO_TypeDef * hx711_SCK_Port, const uint16_t hx711_SCK_Pin, GPIO_TypeDef * hx711_DT_Port, const uint16_t hx711_DT_Pin);
void hx711_Set_Gain(HX711_t * hx711, const HX711_Gain_t new_gain);
bool hx711_Ready(const HX711_t * hx711);

uint32_t __hx711_Download_Data(HX711_t * hx711);
void __hx711_Enable_Disable_DT_PIN_Interrapt(HX711_t * hx711);

uint32_t hx711_Read_Raw(HX711_t * hx711, const uint8_t measurements);
float hx711_Read(HX711_t * hx711, const uint8_t measurements);

void hx711_IT_Interrapt_Disabling(HX711_t * hx711, const IRQn_Type DT_PIN_ItIRQn);
void hx711_IT_Measurements_Enable(HX711_t * hx711, const uint8_t mesurements);
void hx711_IT_Measurements_Disable(HX711_t * hx711);
void hx711_IT_Interrupt_Callback(HX711_t * hx711, const uint16_t GPIO_Pin);
float hx711_IT_Read(HX711_t * hx711, bool * new_mesurement);

void hx711_Tare(HX711_t * hx711, const uint8_t measures);
void hx711_Scale(HX711_t * hx711, const float sample, const uint8_t measurements);

float hx711_Calc(HX711_t * hx711, const uint32_t raw_value);

#endif /* APPLICATION_USER_INC_HX711_H_ */
